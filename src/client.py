import socket


def establish_connection():
    show_prompt()
    connection_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    connection_socket.connect(('localhost', 6700))
    while True:
        message = input("Message: ").encode('utf-8')
        connection_socket.sendall(message)
        reply_from_server = connection_socket.recv(1024).decode('utf-8')
        if reply_from_server == '':
            break
        else:
            print("{str(reply_from_server)}")
    connection_socket.close()


def show_prompt():
    msg = """
    1: Αναζήτηση με όνομα
    2: Αναζήτηση με IP
    3: Εισαγωγή
    4: Διαγραφή
    5: Ενημέρωση
    """
    print(msg)


if __name__ == '__main__':
    establish_connection()

